const fullScreenRenderer = vtk.Rendering.Misc.vtkFullScreenRenderWindow.newInstance({background:[0,0,0,0]});
const vtkColorMaps = vtk.Rendering.Core.vtkColorTransferFunction.vtkColorMaps;

const sliceurls = ['https://jamesbertwistle.gitlab.io/oakvilleL3_demo/static/oakville_L3_ACH.vtp',
                   'https://jamesbertwistle.gitlab.io/oakvilleL3_demo/static/oakville_L3_AoA.vtp',
                   'https://jamesbertwistle.gitlab.io/oakvilleL3_demo/static/oakville_L3_U.vtp',
                ];

const stlurls = [{url: 'https://jamesbertwistle.gitlab.io/oakvilleL3_demo/static/walls.stl', opacity: "0.2"},
                ];

const streamurls = ['https://jamesbertwistle.gitlab.io/oakvilleL3_demo/static/oakville_L3_streams.vtp',
                   ];

var renderer = fullScreenRenderer.getRenderer();
var renderWindow = fullScreenRenderer.getRenderWindow();
renderer.getActiveCamera().setPosition(-10.460718403281259, -41.079852143396536, 45.05497774499336);
renderer.getActiveCamera().setFocalPoint(-10.458100089004823, -41.076304136976084, 45.052607153719684);
renderer.getActiveCamera().setViewUp(0.03938795581347674, 0.5348629214142467, 0.8440202866241149);
renderer.setTwoSidedLighting(true);
renderer.setLightFollowCamera(true);

function displayStl(file) {
    stlurl = file.url;
    var stlMapper = vtk.Rendering.Core.vtkMapper.newInstance({ 
        scalarVisibility: false,
    });
    var stlActor = vtk.Rendering.Core.vtkActor.newInstance();
    var stlReader = vtk.IO.Geometry.vtkSTLReader.newInstance();

    stlActor.getProperty().setOpacity( file.opacity );
    stlActor.setMapper(stlMapper);
    stlMapper.setInputConnection(stlReader.getOutputPort());
    stlActor.getProperty().setColor(1, 1, 1);

    stlReader.setUrl(stlurl).then(() => {
        renderer.addActor(stlActor);
        renderer.resetCamera();
        renderWindow.render();
    });

}

for (var i = stlurls.length -1; i >= 0; i--)
{
    displayStl(stlurls[i]);
}

var lookupTable = vtk.Rendering.Core.vtkColorTransferFunction.newInstance();
var presetU = vtkColorMaps.getPresetByName('Rainbow Desaturated');

var vtkMapper = vtk.Rendering.Core.vtkMapper.newInstance({
        interpolateScalarsBeforeMapping: true,
        useLookupTableScalarRange: true,
        lookupTable,
        scalarVisibility: true,
    });

var vtkActor = vtk.Rendering.Core.vtkActor.newInstance();
var vtpReader = vtk.IO.XML.vtkXMLPolyDataReader.newInstance();

vtkMapper.setInputConnection(vtpReader.getOutputPort());
vtkActor.setMapper(vtkMapper);


vtkMapper.setInputConnection(vtpReader.getOutputPort());
// vtkActor.setMapper(vtkMapper);


fullScreenRenderer.addController('<table>\
    <tr>\
        <td>velPlane</td>\
            <td>\
                <input class="velPlane" type="checkbox" name="velPlane"/>\
            </td>\
        </tr>\
        <tr>\
            <td>ACHPlane</td>\
                <td>\
                    <input class="ACHPlane" type="checkbox" checked name="ACHPlane"/>\
                </td>\
            </tr>\
        <tr>\
            <td>velStream</td>\
                <td>\
                    <input class="velStream" type="checkbox" name="velStream"/>\
                </td>\
            </tr>\
        <tr>\
            <td>AoAPlane</td>\
                <td>\
                    <input class="AoAPlane" type="checkbox" name="AoAPlane"/>\
                </td>\
            </tr>\
        </table>');

var velPlanecheckbox = document.querySelector("input[name=velPlane]")
var AoAPlanecheckbox = document.querySelector("input[name=AoAPlane]")
var velStreamcheckbox = document.querySelector("input[name=velStream]")
var ACHPlanecheckbox = document.querySelector("input[name=ACHPlane]")

velPlanecheckbox.addEventListener('change', function() {
 if(this.checked){
    vtpReader.setUrl(sliceurls[1]).then(() => {
        lookupTable.removeAllPoints();
        lookupTable.applyColorMap(presetU);
        lookupTable.setDiscretize(true);
        lookupTable.setNumberOfValues(16);
        vtkMapper.setScalarRange([0, 10]);
        vtkMapper.setColorByArrayName('magU');
        renderer.addActor(vtkActor);
        renderer.resetCamera();
        renderWindow.render();
    });
    } else {
        renderer.removeActor(vtkActor);
        renderWindow.render();
    }
});
velStreamcheckbox.addEventListener('change', function() {
 if(this.checked){
    vtpReader.setUrl(streamurls[1]).then(() => {
        lookupTable.removeAllPoints();
        lookupTable.applyColorMap(presetU);
        lookupTable.setDiscretize(true);
        lookupTable.setNumberOfValues(16);
        vtkMapper.setScalarRange([0, 10]);
        vtkMapper.setColorByArrayName('magU');
        renderer.addActor(vtkActor);
        renderer.resetCamera();
        renderWindow.render();
    });
    } else {
        renderer.removeActor(vtkActor);
        renderWindow.render();
    }
});

//    var arrayName = 'MeanVelocityMagnitude';
//    // vtkMapper.setScalarModeToUsePointData();
//    vtkMapper.setScalarModeToDefault();
//    vtkMapper.setColorModeToMapScalars();
//    vtkMapper.setColorByArrayName(arrayName);
//    vtkMapper.setScalarRange([0, 1]);

//     var vtkActor = vtk.Rendering.Core.vtkActor.newInstance();
//     var vtpReader = vtk.IO.XML.vtkXMLPolyDataReader.newInstance();

//     vtkMapper.setInputConnection(vtpReader.getOutputPort());
//     vtkActor.setMapper(vtkMapper);

//     function addActors() {
//         renderer.addActor(vtkActor);
//         renderWindow.render();
//     }
//     function removeActors() {
//         renderer.removeActor(vtkActor);
//         renderWindow.render();
//     }

//     if(remove==true){
//         renderer.removeActor(vtkActor);
//         renderWindow.render();
//         console.log(renderer.getActors());
//         // removeActors;
//         // console.log(renderer.getActors());
//         // vtpReader.setUrl(url).then(removeActor);
//     } else {
//         console.log("removefalse");
//         vtpReader.setUrl(url).then(() => {
//             renderer.addActor(vtkActor);
//             renderWindow.render();
//         });
//         console.log(renderer.getActors());
//     }
//     console.log(renderer.getActors());
// }

// displaySlice(sliceurls[1], false)

// fullScreenRenderer.addController('<table><tr><td>velPlane</td><td><input class="velPlane" type="checkbox" name="velPlane"/></td></tr><tr><td>presPlane</td><td><input class="presPlane" type="checkbox" checked name="pressPlane"/></td></tr><tr><td>velStream</td><td><input class="velStream" type="checkbox" name="velStream"/></td></tr><tr><td>presStream</td><td><input class="presStream" type="checkbox" name="presStream"/></td></tr></table>');

// var velPlanecheckbox = document.querySelector("input[name=velPlane]")
// var presPlanecheckbox = document.querySelector("input[name=presPlane]")
// var velStreamcheckbox = document.querySelector("input[name=velStream]")
// var presStreamcheckbox = document.querySelector("input[name=presStream]")

// velPlanecheckbox.addEventListener('change', function() {
//  if(this.checked){
//     displaySlice(sliceurls[0], false)
//  } else {
//     displaySlice(sliceurls[0], true)
//  }
// });
// presPlanecheckbox.addEventListener('change', function() {
//  if(this.checked){
//     displaySlice(sliceurls[1], false)
//  } else {
//     displaySlice(sliceurls[1], true)
//  }
// });
// velStreamcheckbox.addEventListener('change', function() {
//  if(this.checked){
//     displaySlice(streamurls[0], false)
//  } else {
//     displaySlice(streamurls[0], true)
//  }
// });
// presStreamcheckbox.addEventListener('change', function() {
//  if(this.checked){
//     displaySlice(streamurls[1], false)
//  } else {
//     displaySlice(streamurls[1], true)
//  }
// });


//fullScreenRenderer.addController("<table>\
//  <tr>\
//    <td>\
//      <select class='arrays' style='width: 100%'>\
//        <option value='0'>Velocity - Slice </option>\
//        <option value='1'>Pressure - Slice</option>\
//        <option value='2'>Velocity - Streams</option>\
//        <option value='3'>Pressure - Streams</option>\
//        </select>\
//    </td>\
//  </tr></table>");

//var arrayNames = [`c_allyear_${dirs}dirs_LDDC`,
//                    `c_spring_${dirs}dirs_LDDC`,
//                    `c_summer_${dirs}dirs_LDDC`,
//                    `c_autumn_${dirs}dirs_LDDC`,
//                    `c_winter_${dirs}dirs_LDDC`,
//                    `s_allyear_${dirs}dirs_LDDC`];

















